# Running functional Selenium tests without installing Python 3.x and required dependencies:

The Dockerfile contained in the 'development' folder allows you to run BiblioCommons' QA functional Selenium tests without having to install Python 3.x and any required modules, as the tests will be executed from within the Docker container itself, using your local instance of Chrome or Firefox as required.

Pre-requisites:
  - Docker
  - Java JDK or JRE installed

To get started, clone this project:

    git clone git@bitbucket.org:john_kolokotronis/bc-qa-selenium.git

Open a terminal and navigate to the location you cloned the project to, then cd into the 'development' folder:

    cd development/

Then build the Docker image from the Dockerfile:

    docker build .

Once this is done, you are ready to run the Docker container - to do so, you'll need the image ID, which is found in the last line of the `docker build .` output. If you need this at any later time, you can use the command:

    docker image ls

to see any images found on your system and their respective IDs.

You can then start up the container with the following command:

    docker run -v [local_path_to]/bc-qa-selenium:/biblio/qa -w /biblio/qa --add-host="localhost:[host (local workstation) IP]" -p 51001:51001 -it [image ID]

You will need to pass in your path to where you clone the bc-qa-selenium project, your current system IP and the Docker image ID. E.g, on my system, the above command would be formatted as:

    docker run -v /Users/johnkolokotronis/projects/python/bc-qa-selenium:/biblio/qa -w /biblio/qa --add-host="localhost:192.168.110.238" -p 51001:51001 -it 7e17731bf7de

This will drop you into a Bash shell within the Docker container - your terminal output should now look like this:

>root@91e0d987f85e:/biblio/qa#

Before you can run the tests, you need to start an instance of the Selenium Server, running on your local system, instead of the Docker container.

To do so, open a new terminal window and cd back into the 'development' folder. Then start the Selenium Server by running the start-selenium-server.sh shell script:

    ./start-selenium-server.sh

The first time you run this or if you do not have the Selenium Server JAR file and/or the required ChromeDriver/GeckoDriver executables to drive Chrome and Firefox respectively, the script will download and extract the required files to a selenium folder in your home folder (e.g /Users/johnkolokotronis/selenium). It will then launch the Selenium Server on your system and you will see output similar to:

>johnkolokotronis:~/projects/python/bc-qa-selenium/development(core_candidates)$ ./start-selenium-server.sh
>
>[The Selenium Hub is available at: http://localhost:4444/wd/hub/static/resource/hub.html]
>
>11:25:52.385 INFO [GridLauncherV3.parse] - Selenium server version: 3.141.59, revision: e82be7d358
11:25:52.460 INFO [GridLauncherV3.lambda$buildLaunchers$3] - Launching a standalone Selenium Server on port 4444
2019-01-15 11:25:52.500:INFO::main: Logging initialized @306ms to org.seleniumhq.jetty9.util.log.StdErrLog
11:25:52.699 INFO [WebDriverServlet.<init>] - Initialising WebDriverServlet
11:25:52.785 INFO [SeleniumServer.boot] - Selenium Server is up and running on port 4444

You should leave the Selenium Server instance up and running whenever you want to run tests. You can also check the status of the Selenium Server via the console referred to above: http://localhost:4444/wd/hub/static/resource/hub.html.

Now go back to the terminal instance where the Docker container is running and try running a test:

    pytest tests/functional/core/test_C46962.py --alluredir=allure-results

By default, the test will launch a Google Chrome instance on your local machine and run the test. If all goes well, the test will run successfully:

>root@de4d088de3f5:/biblio/qa# pytest tests/functional/core/test_C46962.py --alluredir=allure-results
============================================================== test session starts ===============================================================
platform linux -- Python 3.6.8, pytest-4.2.0, py-1.7.0, pluggy-0.8.1
sensitiveurl: .*
rootdir: /biblio/qa, inifile:
plugins: variables-1.7.1, selenium-1.15.1, metadata-1.8.0, html-1.20.0, base-url-1.4.1, allure-pytest-2.5.5
collected 1 item
>tests/functional/core/test_C46962.py .                                                                                                     [100%]
>
>=========================================================== 1 passed in 30.53 seconds ============================================================

PyTest outputs full failure stack traces to the terminal and at times, it can be hard to decipher why a test has failed exactly. These tests produce results that can generate a rich HTML report, which includes failure screenshots and oher useful information. Within the container, after your tests have run, execute the following command:

    ../../allure/allure-2.7.0/bin/allure serve allure-results/ -p 51001
    
This will produce output like this:

>root@e2f0769de074:/biblio/qa# ../../allure/allure-2.7.0/bin/allure serve allure-results/ -p 51001
Generating report to temp directory...
Report successfully generated to /tmp/7968026914263221267/allure-report
Starting web server...
2019-02-01 21:22:20.214:INFO::main: Logging initialized @1806ms to org.eclipse.jetty.util.log.StdErrLog
Can not open browser because this capability is not supported on your platform. You can use the link below to open the report manually.
Server started at <http://172.17.0.2:51001/>. Press <Ctrl+C> to exit

As you might have guessed, the Docker container does not have a web browser installed, so it fails to open the report automatically. Back on your local machine, open a browser to:

[Allure Report @ Localhost:51001](http://localhost:51001)
 
instead to access the report. The IP provided is actually the container's IP, which is not directly accessible from your host machine. This way you can view the rich HTML reports that the Allure framework generates.

Note that since the bc-qa-selenium project is checked out on your local machine, you can open it in your IDE (PyCharm Community Edition recommended) and make changes as you would normally/add tests, etc and then run them directly from the Docker container. The container removes the need for you install Python 3.x and the required modules/dependencies on your local machine (but you can of course do so if preferred and no longer use the Docker container. Do note that in this scenario, you would still need to run the start-selenium-server shell script to run a Selenium Server instance - this doesn't change regardless where the tests are running from).
