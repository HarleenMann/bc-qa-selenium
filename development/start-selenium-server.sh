#!/usr/bin/env bash

# Make a 'selenium' directory in the user's home folder:
mkdir -p ~/selenium/drivers

# If the Selenium JAR file is not found in the expected location, download a copy:
if [ ! -f ~/selenium/selenium-server-standalone-3.141.59.jar ]; then
    cd ~/selenium
    wget https://selenium-release.storage.googleapis.com/3.141/selenium-server-standalone-3.141.59.jar
fi

# If the ChromeDriver executable is not found in the expect location, download a copy and extract it:
if [ ! -f ~/selenium/drivers/chromedriver ]; then
    cd ~/selenium/drivers
    wget https://chromedriver.storage.googleapis.com/2.45/chromedriver_mac64.zip
    unzip chromedriver_mac64.zip
    rm chromedriver_mac64.zip
fi

# If the GeckoDriver (Firefox) executable is not found in the expect location, download a copy and extract it:
if [ ! -f ~/selenium/drivers/geckodriver ]; then
    cd ~/selenium/drivers
    wget https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-macos.tar.gz
    gunzip -c geckodriver-v0.23.0-macos.tar.gz | tar xopf -
    rm geckodriver-v0.23.0-macos.tar.gz
fi

home_folder=~
# Print out the address of the local Selenium Server:
printf "\n[The Selenium Hub is available at: http://localhost:4444/wd/hub/static/resource/hub.html]\n\n"
# echo "java -Dwebdriver.chrome.driver='$home_folder/selenium/drivers/chromedriver' -Dwebdriver.gecko.driver='$home_folder/selenium/drivers/geckodriver' -jar $home_folder/selenium/selenium-server-standalone-3.141.59.jar"

# Start up the Selenium Server with Chrome and Firefox support:
cd ~/selenium/drivers
java -Dwebdriver.chrome.driver='chromedriver' -Dwebdriver.gecko.driver='geckodriver' -jar $home_folder/selenium/selenium-server-standalone-3.141.59.jar
