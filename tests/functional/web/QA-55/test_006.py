import pytest
import allure
import sys
import sure
from mimesis import Text

sys.path.append('tests')
import configuration.system
import configuration.user

from pages.web.wpadmin.v3.all_taxonomies_page import AllTaxonomiesPage
from pages.web.wpadmin.login import LoginPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("Create New Tags Taxonomy")
@allure.testcase("", "TestRail")

class Test006:
    def test_006(self):
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.name_web, configuration.user.password_web)

        taxonomy_page = AllTaxonomiesPage(self.driver, configuration.system.base_url_web,
                            taxonomy="post_tag").open()
        taxonomy_page.wait.until(lambda s: taxonomy_page.loaded)

        random_text = Text('en').words(quantity=2)
        list_of_words = ' 4546 '.join(random_text)

        taxonomy_page.all_taxonomies.input_name.send_keys(list_of_words)
        taxonomy_page.all_taxonomies.input_slug.send_keys(list_of_words)
        random_description_choice = Text('en').sentence()

        taxonomy_page.all_taxonomies.input_description.send_keys(random_description_choice)
        taxonomy_page.add_new_taxonomy_button.click()
        taxonomy_page.input_search_taxonomies.send_keys(list_of_words)
        taxonomy_page.search_taxonomies_button.click()
        taxonomy_page.wait.until(lambda s: taxonomy_page.table_actions.count_items_top.is_displayed())

        for taxonomy_row in taxonomy_page.taxonomy_rows:
            taxonomy_row.taxonomy_name.text.casefold().should.match(list_of_words.casefold())
