import pytest
import allure
import sure
from mimesis import Person, Internet, Text
import sys
sys.path.append('tests')
import configuration.system
import configuration.user

from pages.web.wpadmin.login import LoginPage
from pages.web.v3.settings_general_tab import SettingsGeneralTabPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("")
@allure.testcase("", "TestRail")
class Test001:
    def test_001(self):
        random = {
            'phone_number': Person('en').telephone(),
            'URL': Internet('en').home_page(),
            'word': Text('en').word(),
            'sentence': Text('en').sentence()
        }

        login_page = LoginPage(self.driver, "https://calgary-stage.bibliocms.com/").open()
        login_page.log_in(configuration.user.name_web, configuration.user.password_web)
        settings_general_tab = SettingsGeneralTabPage(self.driver, "https://calgary-stage.bibliocms.com/", page='bibliocommons-settings', tab='generic').open()
        settings_general_tab.loaded

        elements = [settings_general_tab.library_main_phone_line, settings_general_tab.contact_us_url,
                    settings_general_tab.feedback_button_text, settings_general_tab.feedback_button_url,
                    settings_general_tab.url_to_privacy_page, settings_general_tab.url_to_terms_of_use_page,
                    settings_general_tab.get_a_library_card_link_url, settings_general_tab.staff_catalog_title_label,
                    settings_general_tab.staff_list_label, settings_general_tab.custom_label_for_other_resource,
                    settings_general_tab.city_link, settings_general_tab.city_name,
                    settings_general_tab.other_footer_link, settings_general_tab.other_footer_link_text,
                    settings_general_tab.url_for_support_link, settings_general_tab.facebook,
                    settings_general_tab.twitter, settings_general_tab.flickr,
                    settings_general_tab.instagram, settings_general_tab.youtube,
                    settings_general_tab.pinterest, settings_general_tab.tumblr, settings_general_tab.newsletter]

        values = [random['phone_number'], random['URL'], random['sentence'], "https://calgary.stage.bibliocms.com/submit-your-feedback-2/",
                  random['URL'], random['URL'], "http://biblioux.com/bibliostyles_calgary/header.html", random['word'], random['word'],
                  random['word'], "http://calgarypubliclibrary.com/", "Calgary", "http://biblioux.com/bibliostyles_calgary/header.html",
                  random['sentence'], "http://biblioux.com/bibliostyles_calgary/header.html", random['URL'], random['URL'], random['URL'],
                  random['URL'], random['URL'], random['URL'], random['URL'], random['URL']]

        for element in elements:
            element.clear()

        for i in range(len(elements)):
            elements[i].send_keys(values[i])

        settings_general_tab.enable_make_tags_a_structured_taxonomy.click()
        settings_general_tab.save_changes.click()
        settings_general_tab.wait_for_page_to_load()
        settings_general_tab.loaded

        #Redefining list to avoid StaleElementException
        elements = [settings_general_tab.library_main_phone_line, settings_general_tab.contact_us_url,
                    settings_general_tab.feedback_button_text, settings_general_tab.feedback_button_url,
                    settings_general_tab.url_to_privacy_page, settings_general_tab.url_to_terms_of_use_page,
                    settings_general_tab.get_a_library_card_link_url, settings_general_tab.staff_catalog_title_label,
                    settings_general_tab.staff_list_label, settings_general_tab.custom_label_for_other_resource,
                    settings_general_tab.city_link, settings_general_tab.city_name,
                    settings_general_tab.other_footer_link, settings_general_tab.other_footer_link_text,
                    settings_general_tab.url_for_support_link, settings_general_tab.facebook,
                    settings_general_tab.twitter, settings_general_tab.flickr,
                    settings_general_tab.instagram, settings_general_tab.youtube,
                    settings_general_tab.pinterest, settings_general_tab.tumblr, settings_general_tab.newsletter]

        element_values = []

        for element in elements:
            element_values.append(element.get_attribute("value"))

        #Asserting values from text fields
        for i in range(len(elements)):
            element_values[i].should.be.equal(values[i])

        settings_general_tab.enable_make_tags_a_structured_taxonomy.is_selected().should.be.true
