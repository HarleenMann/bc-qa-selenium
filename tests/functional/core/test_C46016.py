import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

# from pages.core. ... import ...

@pytest.mark.skip(reason = 'TBD')
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46016: Filter search results by Format or other facet")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46016", "TestRail")
class TestC46016:
    def test_C46016(self):
        ('TBD')
