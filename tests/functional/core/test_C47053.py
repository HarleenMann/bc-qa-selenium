import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47053: Click Check availability for 3M eBook")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47053", "TestRail")
class TestC47053:
    def test_C47053(self):
        self.base_url = "https://pickering.demo.bibliocommons.com"
        self.search_term = "Ready Player One"

        home_page = HomePage(self.driver, self.base_url).open()
        search_results_page = home_page.header.search_for(self.search_term)
        search_results_page.wait.until(lambda condition: (len(search_results_page.search_result_items) > 0))
        first_search = search_results_page.search_result_items[0].bib_title.text
        search_results_page.filter_search_results("Ebook").click()
        search_results_page.wait.until(lambda condition: search_results_page.search_result_items[0].bib_title.text != first_search)
        search_results_page.search_result_items[0].check_availability.click()
        search_results_page.wait.until(lambda condition: search_results_page.search_result_items[0].is_availability_status_displayed)
        search_results_page.search_result_items[0].availability_status.text.should.contain("All copies in use")
        search_results_page.search_result_items[0].availability_status.text.should.contain("Holds:")
