from selenium.webdriver.common.by import By
from pypom import Region
from selenium.webdriver.support.expected_conditions import NoSuchElementException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC


class SelectWidgetImage(Region):

    _select_files_button_locator = (By.CSS_SELECTOR, "input[id*='html5']")
    _remove_image_locator = (By.ID, "remove-card-image")
    _add_image_to_widget_button_locator = (By.CSS_SELECTOR, "div.media-toolbar-primary.search-form > button[type='button']")
    _tabs_locator = (By.CSS_SELECTOR, "a.media-menu-item")
    _lists_of_images_locator = (By.CSS_SELECTOR, "ul[id*='attachments-view']")
    _spinner_locator = (By.CSS_SELECTOR, "span[class='spinner']")
    _delete_image_locator = (By.CSS_SELECTOR, "button[class*='delete']")
    _images_in_list_locator = (By.CSS_SELECTOR, "li.attachment.save-ready")
    _close_button_locator = (By.CSS_SELECTOR, "button.media-modal-close")
    _purpose_locator = (By.CSS_SELECTOR, "td > select")
    _purpose_dropdown_options = ["Decorative", "Informative"]

    #Title and Alt Text Locators
    _title_alt_text_locator = (By.CSS_SELECTOR, "label > input")

    #Caption and Description Locator
    _caption_description_locator = (By.CSS_SELECTOR, "label > textarea")

    #Image Attribution Locators
    _image_attribution_checkbox_locator = (By.CSS_SELECTOR, "input[type='checkbox']")
    _image_attribution_text_fields_locator = (By.CSS_SELECTOR, "tr > td > input")

    @property
    def upload_image(self):
        return self.find_element(*self._select_files_button_locator)

    @property
    def remove_image(self):
        return self.find_element(*self._remove_image_locator)

    @property
    def add_image_to_widget_button(self):
        add_image_to_widget_buttons = self.find_elements(*self._add_image_to_widget_button_locator)
        if len(add_image_to_widget_buttons) == 1:
            self.wait.until(EC.element_to_be_clickable(self._add_image_to_widget_button_locator))
            return add_image_to_widget_buttons[0]
        else:
            return add_image_to_widget_buttons[len(add_image_to_widget_buttons)-1]

    @property
    def media_library_tab(self):
        tabs = self.find_elements(*self._tabs_locator)
        return tabs[len(tabs)-1]

    @property
    def upload_files_tab(self):
        tabs = self.find_elements(*self._tabs_locator)
        return tabs[len(tabs)-2]

    def select_image_from_list(self, image_position):
        self.lists = self.find_elements(*self._lists_of_images_locator)
        self.image = self.lists[len(self.lists)-1]
        images = self.image.find_elements(*self._images_in_list_locator)

        while(len(images) == 0):
            self.lists = self.find_elements(*self._lists_of_images_locator)
            self.image = self.lists[len(self.lists)-1]
            images = self.image.find_elements(*self._images_in_list_locator)
            if(len(images) > 0):
                return images[image_position]

        return images[image_position]

    @property
    def delete_image_link(self):
        delete_link = self.find_elements(*self._delete_image_locator)
        return delete_link[len(delete_link)-1]

    def accept_alert_to_delete_image(self):
        alert = self.wait.until(EC.alert_is_present())
        alert.accept()

    @property
    def close_button(self):
        close = self.find_elements(*self._close_button_locator)
        return close[len(close)-1]

    @property
    def title(self):
        image_titles = self.find_elements(*self._title_alt_text_locator)
        return image_titles[len(image_titles)-2]

    @property
    def caption(self):
        elements = self.find_elements(*self._caption_description_locator)
        return elements[len(elements)-2]

    @property
    def alt_text(self):
        image_titles = self.find_elements(*self._title_alt_text_locator)
        return image_titles[len(image_titles)-1]

    @property
    def description(self):
        elements = self.find_elements(*self._caption_description_locator)
        return elements[len(elements)-1]

    @property
    def purpose(self):
        return self.find_element(*self._purpose_locator)

    def select_purpose(self, value):
        select = Select(self.purpose)
        if value in self._purpose_dropdown_options:
            select.select_by_visible_text(value)
        else:
            raise NoSuchElementException("Option not found.")

    @property
    def image_attribution(self):
        element = self.find_elements(*self._image_attribution_checkbox_locator)
        if element[len(element)-9].is_enabled():
            return element[len(element)-9]
        else:
            raise NoSuchElementException

    @property
    def image_title(self):
        element = self.find_elements(*self._image_attribution_text_fields_locator)
        return element[len(element)-7]

    @property
    def source_url(self):
        element = self.find_elements(*self._image_attribution_text_fields_locator)
        return element[len(element)-6]

    @property
    def author(self):
        element = self.find_elements(*self._image_attribution_text_fields_locator)
        return element[len(element)-5]

    @property
    def author_url(self):
        element = self.find_elements(*self._image_attribution_text_fields_locator)
        return element[len(element)-4]

    @property
    def cc_license_version(self):
        element = self.find_elements(*self._image_attribution_text_fields_locator)
        return element[len(element)-3]

    @property
    def cc_license_url(self):
        element = self.find_elements(*self._image_attribution_text_fields_locator)
        return element[len(element)-2]

    @property
    def modification_made(self):
        element = self.find_elements(*self._image_attribution_text_fields_locator)
        return element[len(element)-1]
