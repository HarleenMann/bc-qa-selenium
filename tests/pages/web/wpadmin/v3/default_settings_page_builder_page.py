from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select

from pages.web.staff_base import StaffBasePage

class DefaultSettingsPageBuilderPage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/edit.php"

    # Default Settings Heading and Help Text locators
    _default_settings_heading_locator = (By.CSS_SELECTOR, "[data-key='settings-title']")
    _help_text_locator = (By.CLASS_NAME, "fm-item-description")

    # Set Homepage Locators
    _set_homepage_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-page_on_front-0']")
    _set_homepage_dropdown_locator = (By.CSS_SELECTOR, "div.chosen-container")

    # Rows section locators
    _rows_heading_locator = (By.CSS_SELECTOR, "[data-key='settings-heading-rows']")
    _row_width_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_row_width_default-0']")
    _row_width_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-row-width']")
    _fixed_width_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_row_width-0']")
    _fixed_width_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-fixed-width']")

    # Font sizes section locators
    _font_sizes_heading_locator = (By.CSS_SELECTOR, "[data=key='settings-heading-fonts']")
    _heading_modules_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_row_heading_font_size-0']")
    _font_size_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-module-heading']")
    _standard_card_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_title_font_size-0']")
    _standard_card_title_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-card-title']")
    _structured_large_card_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_title_font_size_structured_large-0']")
    _structured_large_card_title_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-card-title-large']")

    # Information Display section locators
    _information_display_heading_locator = (By.CSS_SELECTOR, "[data-key='fm-default_page_builder_settings-0-bw_bb_card_title_font_size_structured_large-0']")
    _card_description_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_show_description-0']")
    _card_description_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-card-description']")
    _display_taxonomy_links_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_show_tax-0']")
    _display_taxonomy_links_checkbox_locator = (By.CSS_SELECTOR, "[data-key='settings-display-taxonomy-link']")
    _content_type_locator = (By.ID, "fm-default_page_builder_settings-0-bw_bb_card_show_tax-0-bw_tax_content_type")
    _audience_locator = (By.ID, "fm-default_page_builder_settings-0-bw_bb_card_show_tax-0-bw_audience_category")
    _related_format_locator = (By.ID, "fm-default_page_builder_settings-0-bw_bb_card_show_tax-0-bw_related_format")
    _programs_campaigns_locator = (By.ID, "fm-default_page_builder_settings-0-bw_bb_card_show_tax-0-bc_program")
    _genre_locator = (By.ID, "fm-default_page_builder_settings-0-bw_bb_card_show_tax-0-bw_tax_genre")
    _topic_locator = (By.ID, "fm-default_page_builder_settings-0-bw_bb_card_show_tax-0-bw_tax_topic")
    _tag_locator = (By.ID, "fm-default_page_builder_settings-0-bw_bb_card_show_tax-0-post_tag")

    # Card Design & Images section Locators
    _card_design_heading_locator = (By.CSS_SELECTOR, "[data-key='settings-heading-card']")
    _image_position_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_image_position-0']")
    _image_position_dropdown_locator = (By.CSS_SELECTOR, "[data-key= 'settings-card-image-position']")
    _border_style_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_show_border-0']")
    _border_style_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-card-border-style']")
    _tiled_card_border_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_show_border_tiled-0']")
    _tiled_card_border_dropdown_locator = (By.CSS_SELECTOR, "[data-key= 'settings-card-border-style-tiled']")
    _poll_card_style_label_locator = ((By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_card_show_response_options-0']"))
    _poll_card_style_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-card-poll-style']")

    # Buttons section locators
    _setting_buttons_heading_locator = (By.CSS_SELECTOR, "[data-key='settings-heading-buttons']")
    _button_style_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_button_style-0']")
    _button_style_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-button-style']")
    _background_hover_color_label_locator = (By.CSS_SELECTOR, "[for='fm-default_page_builder_settings-0-bw_bb_button_style-0']")
    _background_hover_color_dropdown_locator = (By.CSS_SELECTOR, "[data-key='settings-button-background-hover']")

    # Save changes button locator
    _save_changes_button_locator = (By.ID, "fm-submit")

    _verify_set_homepage_dropdown_locator = (By.CLASS_NAME, "chosen-single")

    dropdown_size_options = ["Small", "Modest", "Medium", "Moderate", "Large", "Giant"]
    dropdown_border_style_options = ["Show Border", "Hide Border"]

    def loaded(self):
        try:
            return self.find_element(*self._default_settings_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    def is_heading_locator_displayed(self):
        return self.find_element(*self._default_settings_heading_locator)

    def is_help_text_displayed(self):
        return self.find_element(*self._help_text_locator)

    def get_home_page_locator_element(self):
        return self.find_element(*self._set_homepage_dropdown_locator)

    def select_home_page_locator_dropdown(self, value):
        self.wait.until(lambda s: self.loaded)
        dropdown_option_locator = self.get_home_page_locator_element()
        dropdown_option_locator.click()
        options = self.driver.find_elements(By.CSS_SELECTOR, "li.active-result")

        found = False
        for option in options:
            if option.text == value:
                option.click()
                found = True
                break
        if found is False:
            raise NoSuchElementException("No option(s) found")

    def get_row_width_locator_element(self):
        return self.find_element(*self._row_width_dropdown_locator)

    def is_fixed_width_element_displayed(self):
        try:
            return self.find_element(*self._fixed_width_label_locator).is_displayed()
        except NoSuchElementException:
            return False

    def select_row_width_dropdown(self, value, pixels):
        sel = Select(self.get_row_width_locator_element())

        if value == 'full':
            sel.select_by_value(value)
        elif value == 'fixed':
            sel.select_by_value(value)
            self.wait.until(lambda s: self.is_fixed_width_element_displayed())
            fixed_width_variable = self.find_element(*self._fixed_width_dropdown_locator)
            fixed_width_variable.clear()
            fixed_width_variable.send_keys(pixels)
        else:
         raise NoSuchElementException("No option(s) found")

    def get_heading_module_selecter_element(self):
        return self.find_element(*self._font_size_dropdown_locator)

    def select_heading_module_dropdown(self, value):
        sel = Select(self.get_heading_module_selecter_element())
        if value in self.dropdown_size_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def get_standard_card_title_locator_element(self):
         return self.find_element(*self._standard_card_title_dropdown_locator)

    def select_standard_card_title_dropdown(self, value):
        sel = Select(self.get_standard_card_title_locator_element())
        if value in self.dropdown_size_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def get_structured_card_title_locator_element(self):
        return self.find_element(*self._structured_large_card_title_dropdown_locator)

    def select_structured_card_title_dropdown(self, value):
        sel = Select(self.get_structured_card_title_locator_element())
        if value in self.dropdown_size_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def get_card_description_locator_element(self):
        return self.find_element(*self._card_description_dropdown_locator)

    def select_card_description_dropdown(self, value):
        card_description_dropdown_options = ['Show Description', 'Hide Description']
        sel = Select(self.get_card_description_locator_element())
        if value in card_description_dropdown_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def select_taxonomy_links_dropdown(self, values):
        taxonomy_terms = {
            'Content Types': self._content_type_locator,
            'Audience': self._audience_locator,
            'Related Format': self._related_format_locator,
            'Programs & Campaigns': self._programs_campaigns_locator,
            'Genre': self._genre_locator,
            'Topic': self._topic_locator,
            'Tags': self._tag_locator
        }

        for term in values:
            try:
                select_checkbox = taxonomy_terms.get(term)
                checkbox = self.find_element(*select_checkbox)
                if not checkbox.is_selected():
                    checkbox.click()
            except NoSuchElementException:
                raise NoSuchElementException(" No option(s) found ")
            except TypeError:
                raise NoSuchElementException(" Wrong value given in input data ")

    def get_image_position_locator_element(self):
        return self.find_element(*self._image_position_dropdown_locator)

    def select_image_position_dropdown(self, value):
        dropdown_image_position_options = ['Above Text', 'Below Text']
        sel = Select(self.get_image_position_locator_element())
        if value in dropdown_image_position_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def get_border_style_locator_element(self):
        return self.find_element(*self._border_style_dropdown_locator)

    def select_border_style_dropdown(self, value):
        sel = Select(self.get_border_style_locator_element())
        if value in self.dropdown_border_style_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def get_tiled_card_border_locator_element(self):
        return self.find_element(*self._tiled_card_border_dropdown_locator)

    def select_tiled_card_border_dropdown(self, value):
        sel = Select(self.get_tiled_card_border_locator_element())
        if value in self.dropdown_border_style_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def get_poll_card_style_locator_element(self):
        return self.find_element(*self._poll_card_style_dropdown_locator)

    def select_poll_card_style_dropdown(self, value):
        dropdown_poll_card_options = ['Response Options Open', 'Response Options Closed']
        sel = Select(self.get_poll_card_style_locator_element())
        if value in dropdown_poll_card_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException(" No option(s) got selected ")

    def get_button_style_locator_element(self):
        return self.find_element(*self._button_style_dropdown_locator)

    def select_button_style_dropdown(self, value):
        dropdown_button_style_options = ['Solid', 'Ghost']
        sel = Select(self.get_button_style_locator_element())
        if value in dropdown_button_style_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def get_background_hover_color_locator_element(self):
        return self.find_element(*self._background_hover_color_dropdown_locator)

    def select_background_hover_color_dropdown(self, value):
        dropdown_hover_color_options = ['Darken 20%', 'Lighten 20%']
        sel = Select(self.get_background_hover_color_locator_element())
        if value in dropdown_hover_color_options:
            sel.select_by_visible_text(value)
        else:
            raise NoSuchElementException("No option(s) found")

    def save_changes_button_click(self):
        self.find_element(*self._save_changes_button_locator).click()

    def verify_set_homepage_dropdown(self):
        return self.find_element(*self._verify_set_homepage_dropdown_locator)