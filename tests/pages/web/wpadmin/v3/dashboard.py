from pypom import Region
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from pages.web.staff_base import StaffBasePage

class DashboardPage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/index.php"

    _page_heading_locator = (By.CSS_SELECTOR, "[data-key='dashboard-title']")
    _view_all_cards_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-view-all']")
    _view_my_cards_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-view-mine']")
    _block_locator = (By.CSS_SELECTOR, "[data-key='dashboard-block']")
    _section_card_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-blocks']")
    _section_featured_locator = (By.CSS_SELECTOR, "[data-key='dashboard-featured-blocks']")
    _section_content_locator = (By.CSS_SELECTOR, "[data-key='dashboard-content-blocks']")
    _section_card_heading_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-title']")
    _section_featured_heading_locator = (By.CSS_SELECTOR, "[data-key='dashboard-featured-title']")
    _section_content_heading_locator = (By.CSS_SELECTOR, "[data-key='dashboard-content-title']")

    @property
    def page_heading(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def is_page_heading_displayed(self):
        return self.find_element(*self._page_heading_locator).is_displayed()

    @property
    def view_all_cards_link(self):
        return self.find_element(*self._view_all_cards_link_locator)

    @property
    def view_my_cards_link(self):
        return self.find_element(*self._view_my_cards_link_locator)

    @property
    def section_card(self):
        return self.find_element(*self._section_card_locator)

    @property
    def section_featured(self):
        return self.find_element(*self._section_featured_locator)

    @property
    def section_content(self):
        return self.find_element(*self._section_content_locator)

    @property
    def section_card_heading(self):
        return self.find_element(*self._section_card_heading_locator)

    @property
    def section_featured_heading(self):
        return self.find_element(*self._section_featured_heading_locator)

    @property
    def section_content_heading(self):
        return self.find_element(*self._section_content_heading_locator)

    @property
    def block(self):
        return self.find_element(*self._block_locator)

    @property
    def all_blocks(self):
        return [self.Block(self, element) for element in self.find_elements(*self._block_locator)]

    @property
    def card_blocks(self):
        return [self.Block(self, element) for element in (self.section_card).find_elements(*self._block_locator)]

    @property
    def featured_blocks(self):
        return [self.Block(self, element) for element in (self.section_featured).find_elements(*self._block_locator)]

    @property
    def content_blocks(self):
        return [self.Block(self, element) for element in (self.section_content).find_elements(*self._block_locator)]

    class Block(Region):

        _title_locator = (By.CSS_SELECTOR, "[data-key='dashboard-block-title']")

        # Toggle
        _toggle_locator = (By.CSS_SELECTOR, "[data-key='dashboard-block-toggle']")
        _toggle_collapsed_locator = (By.CSS_SELECTOR, "[class='js-dashboard-console__more c-dashboard-console__more'] > span")
        _toggle_expanded_locator = (By.CSS_SELECTOR, "[class='js-dashboard-console__less c-dashboard-console__less'] > span")

        # Menu
        _menu_locator = (By.CSS_SELECTOR, "[data-key='dashboard-block-menu']")
        _menu_heading_locator = (By.CSS_SELECTOR, "[data-key='dashboard-block-menu-title']")
        _menu_column_locator = (By.CSS_SELECTOR, "[data-key='dashboard-block-menu-column']")

        # Create New button
        _card_blog_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-blog-create']")
        _card_news_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-news-create']")
        _card_list_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-list-create']")
        _card_comment_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-comment-create']")
        _card_event_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-event-create']")
        _card_resource_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-resource-create']")
        _card_poll_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-poll-create']")
        _card_twitter_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-twitter-create']")
        _card_custom_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-custom-create']")
        _featured_banner_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-banner-create']")
        _featured_hero_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-slide-hero-create']")
        _content_page_standard_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-standard-create']")
        _content_page_custom_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-custom-create']")
        _content_blog_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-create']")
        _content_news_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-news-create']")
        _content_faq_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-create']")
        _content_resource_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-create']")
        _content_archive_collection_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-collection-create']")
        _content_special_content_create_new_button_locator = (By.CSS_SELECTOR, "[data-key='dashboard-content-create']")

        # View All link
        _card_blog_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-blog-view-all']")
        _card_news_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-news-view-all']")
        _card_list_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-list-view-all']")
        _card_comment_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-comment-view-all']")
        _card_event_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-event-view-all']")
        _card_resource_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-resource-view-all']")
        _card_poll_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-poll-view-all']")
        _card_twitter_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-twitter-view-all']")
        _card_custom_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-card-custom-view-all']")
        _featured_banner_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-banner-view-all']")
        _featured_hero_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-slide-hero-view-all']")
        _content_page_standard_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-standard-view-all']")
        _content_page_custom_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-custom-view-all']")
        _content_blog_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-view-all']")
        _content_news_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-news-view-all']")
        _content_faq_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-view-all']")
        _content_resource_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-view-all']")
        _content_archive_collection_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-content-view-all']")
        _content_special_content_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-content-view-all']")

        # View Mine link
        _featured_banner_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-banner-view-mine']")
        _featured_hero_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-slide-hero-view-mine']")
        _content_page_standard_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-standard-view-mine']")
        _content_page_custom_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-custom-view-mine']")
        _content_blog_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-view-mine']")
        _content_news_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-news-view-mine']")
        _content_faq_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-view-mine']")
        _content_resource_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-view-mine']")
        _content_archive_collection_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-content-view-mine']")
        _content_special_content_view_mine_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-content-view-mine']")

        @property
        def title(self):
            return self.find_element(*self._title_locator)

        # Toggle
        @property
        def toggle(self):
            return self.find_element(*self._toggle_locator)

        @property
        def is_toggle_collapsed(self):
            try:
                return self.find_element(*self._toggle_collapsed_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def is_toggle_expanded(self):
            try:
                return self.find_element(*self._toggle_expanded_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def toggle_collapsed(self):
            return self.find_element(*self._toggle_collapsed_locator)

        @property
        def toggle_expanded(self):
            return self.find_element(*self._toggle_expanded_locator)

        # Block menu
        @property
        def menu(self):
            return self.find_element(*self._menu_locator)

        @property
        def is_menu_displayed(self):
            try:
                return self.find_element(*self._menu_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def menu_heading(self):
            return self.find_element(*self._menu_heading_locator)

        @property
        def menu_column(self):
            return self.find_element(*self._menu_column_locator)

        @property
        def menu_all_columns(self):
            return [self.BlockMenuColumn(self, element) for element in self.find_elements(*self._menu_column_locator)]

        # Buttons - Create New
        @property
        def create_new_button_card_blog(self):
            return self.find_element(*self._card_blog_create_new_button_locator)

        @property
        def create_new_button_card_news(self):
            return self.find_element(*self._card_news_create_new_button_locator)

        @property
        def create_new_button_card_list(self):
            return self.find_element(*self._card_list_create_new_button_locator)

        @property
        def create_new_button_card_comment(self):
            return self.find_element(*self._card_comment_create_new_button_locator)

        @property
        def create_new_button_card_event(self):
            return self.find_element(*self._card_event_create_new_button_locator)

        @property
        def create_new_button_card_resource(self):
            return self.find_element(*self._card_resource_create_new_button_locator)

        @property
        def create_new_button_card_poll(self):
            return self.find_element(*self._card_poll_create_new_button_locator)

        @property
        def create_new_button_card_twitter(self):
            return self.find_element(*self._card_twitter_create_new_button_locator)

        @property
        def create_new_button_card_custom(self):
            return self.find_element(*self._card_custom_create_new_button_locator)

        @property
        def create_new_button_featured_banner(self):
            return self.find_element(*self._featured_banner_create_new_button_locator)

        @property
        def create_new_button_featured_hero(self):
            return self.find_element(*self._featured_hero_create_new_button_locator)

        @property
        def create_new_button_content_page_standard(self):
            return self.find_element(*self._content_page_standard_create_new_button_locator)

        @property
        def create_new_button_content_page_custom(self):
            return self.find_element(*self._content_page_custom_create_new_button_locator)

        @property
        def create_new_button_content_blog(self):
            return self.find_element(*self._content_blog_create_new_button_locator)

        @property
        def create_new_button_content_news(self):
            return self.find_element(*self._content_news_create_new_button_locator)

        @property
        def create_new_button_content_faq(self):
            return self.find_element(*self._content_faq_create_new_button_locator)

        @property
        def create_new_button_content_resource(self):
            return self.find_element(*self._content_resource_create_new_button_locator)

        @property
        def create_new_button_content_archive_collection(self):
            return self.find_element(*self._content_archive_collection_create_new_button_locator)

        @property
        def create_new_button_content_special_content(self):
            return self.find_element(*self._content_special_content_create_new_button_locator)

        # Links - View All
        @property
        def view_all_link_card_blog(self):
            return self.find_element(*self._card_blog_view_all_link_locator)

        @property
        def view_all_link_card_news(self):
            return self.find_element(*self._card_news_view_all_link_locator)

        @property
        def view_all_link_card_list(self):
            return self.find_element(*self._card_list_view_all_link_locator)

        @property
        def view_all_link_card_comment(self):
            return self.find_element(*self._card_comment_view_all_link_locator)

        @property
        def view_all_link_card_event(self):
            return self.find_element(*self._card_event_view_all_link_locator)

        @property
        def view_all_link_card_resource(self):
            return self.find_element(*self._card_resource_view_all_link_locator)

        @property
        def view_all_link_card_poll(self):
            return self.find_element(*self._card_poll_view_all_link_locator)

        @property
        def view_all_link_card_twitter(self):
            return self.find_element(*self._card_twitter_view_all_link_locator)

        @property
        def view_all_link_card_custom(self):
            return self.find_element(*self._card_custom_view_all_link_locator)

        @property
        def view_all_link_featured_banner(self):
            return self.find_element(*self._featured_banner_view_all_link_locator)

        @property
        def view_all_link_featured_hero(self):
            return self.find_element(*self._featured_hero_view_all_link_locator)

        @property
        def view_all_link_content_page_standard(self):
            return self.find_element(*self._content_page_standard_view_all_link_locator)

        @property
        def view_all_link_content_page_custom(self):
            return self.find_element(*self._content_page_custom_view_all_link_locator)

        @property
        def view_all_link_content_blog(self):
            return self.find_element(*self._content_blog_view_all_link_locator)

        @property
        def view_all_link_content_news(self):
            return self.find_element(*self._content_news_view_all_link_locator)

        @property
        def view_all_link_content_faq(self):
            return self.find_element(*self._content_faq_view_all_link_locator)

        @property
        def view_all_link_content_resource(self):
            return self.find_element(*self._content_resource_view_all_link_locator)

        @property
        def view_all_link_content_archive_collection(self):
            return self.find_element(*self._content_archive_collection_view_all_link_locator)

        @property
        def view_all_link_content_special_content(self):
            return self.find_element(*self._content_special_content_view_all_link_locator)

        # Links - View Mine
        @property
        def view_mine_link_featured_banner(self):
            return self.find_element(*self._featured_banner_view_mine_link_locator)

        @property
        def view_mine_link_featured_hero(self):
            return self.find_element(*self._featured_hero_view_mine_link_locator)

        @property
        def view_mine_link_content_page_standard(self):
            return self.find_element(*self._content_page_standard_view_mine_link_locator)

        @property
        def view_mine_link_content_page_custom(self):
            return self.find_element(*self._content_page_custom_view_mine_link_locator)

        @property
        def view_mine_link_content_blog(self):
            return self.find_element(*self._content_blog_view_mine_link_locator)

        @property
        def view_mine_link_content_news(self):
            return self.find_element(*self._content_news_view_mine_link_locator)

        @property
        def view_mine_link_content_faq(self):
            return self.find_element(*self._content_faq_view_mine_link_locator)

        @property
        def view_mine_link_content_resource(self):
            return self.find_element(*self._content_resource_view_mine_link_locator)

        @property
        def view_mine_link_content_archive_collection(self):
            return self.find_element(*self._content_archive_collection_view_mine_link_locator)

        @property
        def view_mine_link_content_special_content(self):
            return self.find_element(*self._content_special_content_view_mine_link_locator)

        class BlockMenuColumn(Region):

            _subheading_locator = (By.CSS_SELECTOR, "[data-key='dashboard-block-menu-column-title']")

            # Standard Page
            _page_standard_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-standard-menu-create']")
            _page_standard_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-standard-menu-view-all']")

            # Custom Page
            _page_custom_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-custom-menu-create']")
            _page_custom_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-page-custom-menu-view-all']")

            # Blog Post
            _blog_post_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-menu-create']")
            _blog_post_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-menu-view-all']")

            _blog_post_menu_comments_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-comments']")
            _blog_post_menu_my_comments_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-comments-mine']")
            _blog_post_menu_my_unread_comments_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-blog-comments-mine-unread']")

            # News Post
            _news_post_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-news-menu-create']")
            _news_post_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-news-menu-view-all']")
            _news_post_menu_settings_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-news-settings']")

            # FAQs
            _faqs_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-menu-create']")
            _faqs_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-menu-view-all']")
            _faqs_menu_settings_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-settings']")

            _faqs_menu_categories_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-tax-categories']")
            _faqs_menu_categories_order_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-faq-tax-categories-order']")

            # Online Resources
            _resource_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-menu-create']")
            _resource_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-menu-view-all']")
            _resource_menu_settings_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-settings']")

            _resource_menu_subject_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-tax-subject']")
            _resource_menu_learning_tool_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-tax-learning-tool']")
            _resource_menu_subject_kids_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-tax-subject-kids']")
            _resource_menu_subject_teen_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-tax-subject-teen']")
            _resource_menu_format_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-resource-tax-format']")

            # Archive Collections
            _archive_collection_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-collection-menu-create']")
            _archive_collection_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-collection-menu-view-all']")
            _archive_collection_menu_settings_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-collection-settings']")

            _archive_collection_menu_subject_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-collection-tax-subject']")

            # Special Content
            _special_content_menu_create_new_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-special-menu-create']")
            _special_content_menu_view_all_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-special-menu-view-all']")
            _special_content_menu_settings_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-special-settings']")

            _special_content_menu_categories_link_locator = (By.CSS_SELECTOR, "[data-key='dashboard-special-tax-categories']")

            @property
            def subheading(self):
                return self.find_element(*self._subheading_locator)

            # Links - Create New
            @property
            def create_new_link_menu_page_standard(self):
                return self.find_element(*self._page_standard_menu_create_new_link_locator)

            @property
            def create_new_link_menu_page_custom(self):
                return self.find_element(*self._page_custom_menu_create_new_link_locator)

            @property
            def create_new_link_menu_blog_post(self):
                return self.find_element(*self._blog_post_menu_create_new_link_locator)

            @property
            def create_new_link_menu_news_post(self):
                return self.find_element(*self._news_post_menu_create_new_link_locator)

            @property
            def create_new_link_menu_faqs(self):
                return self.find_element(*self._faqs_menu_create_new_link_locator)

            @property
            def create_new_link_menu_resource(self):
                return self.find_element(*self._resource_menu_create_new_link_locator)

            @property
            def create_new_link_menu_archive_collection(self):
                return self.find_element(*self._archive_collection_menu_create_new_link_locator)

            @property
            def create_new_link_menu_special_content(self):
                return self.find_element(*self._special_content_menu_create_new_link_locator)

            # Links - View All
            @property
            def view_all_link_menu_page_standard(self):
                return self.find_element(*self._page_standard_menu_view_all_link_locator)

            @property
            def view_all_link_menu_page_custom(self):
                return self.find_element(*self._page_custom_menu_view_all_link_locator)

            @property
            def view_all_link_menu_blog_post(self):
                return self.find_element(*self._blog_post_menu_view_all_link_locator)

            @property
            def view_all_link_menu_news_post(self):
                return self.find_element(*self._news_post_menu_view_all_link_locator)

            @property
            def view_all_link_menu_faqs(self):
                return self.find_element(*self._faqs_menu_view_all_link_locator)

            @property
            def view_all_link_menu_resource(self):
                return self.find_element(*self._resource_menu_view_all_link_locator)

            @property
            def view_all_link_menu_archive_collection(self):
                return self.find_element(*self._archive_collection_menu_view_all_link_locator)

            @property
            def view_all_link_menu_special_content(self):
                return self.find_element(*self._special_content_menu_view_all_link_locator)

            # Links - Settings
            @property
            def settings_link_menu_news_post(self):
                return self.find_element(*self._news_post_menu_settings_link_locator)

            @property
            def settings_link_menu_faqs(self):
                return self.find_element(*self._faqs_menu_settings_link_locator)

            @property
            def settings_link_menu_resource(self):
                return self.find_element(*self._resource_menu_settings_link_locator)

            @property
            def settings_link_menu_archive_collection(self):
                return self.find_element(*self._archive_collection_menu_settings_link_locator)

            @property
            def settings_link_menu_special_content(self):
                return self.find_element(*self._special_content_menu_settings_link_locator)

            # Links - Blog Comments
            @property
            def blog_post_all_comments_link(self):
                return self.find_element(*self._blog_post_menu_comments_link_locator)

            @property
            def blog_post_my_comments_link(self):
                return self.find_element(*self._blog_post_menu_my_comments_link_locator)

            @property
            def blog_post_my_unread_comments_link(self):
                return self.find_element(*self._blog_post_menu_my_unread_comments_link_locator)

            # Links - FAQs Taxonomies
            @property
            def faqs_categories_link(self):
                return self.find_element(*self._faqs_menu_categories_link_locator)

            @property
            def faqs_categories_order_link(self):
                return self.find_element(*self._faqs_menu_categories_order_link_locator)

            # Links - Online Resources Taxonomies
            @property
            def resource_subject_link(self):
                return self.find_element(*self._resource_menu_subject_link_locator)

            @property
            def resource_learning_tool_link(self):
                return self.find_element(*self._resource_menu_learning_tool_link_locator)

            @property
            def resource_subject_kids_link(self):
                return self.find_element(*self._resource_menu_subject_kids_link_locator)

            @property
            def resource_subject_teen_link(self):
                return self.find_element(*self._resource_menu_subject_teen_link_locator)

            @property
            def resource_format_link(self):
                return self.find_element(*self._resource_menu_format_link_locator)

            # Links - Archive Collection Taxonomies
            @property
            def archive_collection_subjects_link(self):
                return self.find_element(*self._archive_collection_menu_subject_link_locator)

            # Links - Special Content Taxonomies
            @property
            def special_content_categories_link(self):
                return self.find_element(*self._special_content_menu_categories_link_locator)
