from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from pypom import Region

class OnHoldItem(Region):

    _pause_locator = (By.CSS_SELECTOR, "[data-classes='suspend_inline_overlay']")
    _cancel_locator = (By.CSS_SELECTOR, "[data-classes='cancel_hold_inline_overlay']")
    _suspend_end_date_locator = (By.CSS_SELECTOR, "[data-test-id='suspend-end-date-picker']")
    _pause_holds_locator = (By.CSS_SELECTOR, "[data-test-id='edit-suspend-date-submit-button']")
    _cancel_holds_locator = (By.CSS_SELECTOR, "[data-test-id='button-confirm-continue']")
    _pickup_branch_locator = (By.CSS_SELECTOR, "[testid='select_pickupBranch']")

    @property
    def pause(self):
        return self.find_element(*self._pause_locator)

    @property
    def cancel(self):
        return self.find_element(*self._cancel_locator)

    @property
    def suspend_end_date(self):
        return self.find_element(*self._suspend_end_date_locator)

    @property
    def is_suspend_end_date_displayed(self):
        try:
            return self.find_element(*self._suspend_end_date_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def pause_holds(self):
        return self.find_element(*self._pause_holds_locator)

    @property
    def is_pause_holds_displayed(self):
        try:
            return self.find_element(*self._pause_holds_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def cancel_holds(self):
        return self.find_element(*self._cancel_holds_locator)

    @property
    def is_cancel_holds_displayed(self):
        try:
            return self.find_element(*self._cancel_holds_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def pickup_branch(self):
        return Select(self.find_element(*self._pickup_branch_locator))


