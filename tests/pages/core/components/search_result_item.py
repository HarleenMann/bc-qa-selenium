from selenium.webdriver.common.by import By
from pypom import Region
from selenium.common.exceptions import NoSuchElementException

class SearchResultItem(Region):

    _bib_title_locator = (By.CSS_SELECTOR, "[data-key='bib-title']")
    _bib_subtitle_locator = (By.CLASS_NAME, "cp-subtitle")
    _bib_title_text_locator = (By.CSS_SELECTOR, "[data-key='bib-title'] > span")
    _bib_author_locator = (By.CLASS_NAME, "author-link")
    _add_to_shelf_combo_button_locator = (By.CLASS_NAME, "cp-add-to-shelf-combo-button")
    _add_to_shelf_success_message_locator = (By.CLASS_NAME, "cp-alert-success")
    _on_shelf_button_locator = (By.CLASS_NAME, "cp-on-shelf-button")
    # _on_shelf_dropdown_button_locator = (By.CLASS_NAME, "cp-on-shelf-dropdown")
    _on_shelf_dropdown_button_locator = (By.CLASS_NAME, "cp-dropdown-trigger")
    _remove_from_shelves_locator = (By.CLASS_NAME, "shelf-delete")
    _request_this_download_locator = (By.CSS_SELECTOR, "[data-key='bib-request-this-download']")
    _place_hold_locator = (By.CSS_SELECTOR, "[data-key='bib-place-a-hold']")
    _place_digital_hold_locator = (By.CSS_SELECTOR, "button.cp-btn.btn.cp-primary-btn.btn-primary.pull-right")
    _pickup_location_locator = (By.CLASS_NAME, "selected-value")
    _alert_locator = (By.CLASS_NAME, "alert-content")
    _item_format_text_locator = (By.CSS_SELECTOR, "[data-test-id='format-indicator'] > span")
    _check_availability_locator = (By.CSS_SELECTOR, "[class*='check-availability-button']")
    _availability_status_locator = (By.CLASS_NAME, "cp-digital-availability-block")
    _grouped_search_item_locator = (By.CSS_SELECTOR, ".cp-manifestation-list-item.row")

    _confirm_hold_locator = (By.CSS_SELECTOR, "[data-test-id*='submit-place-hold-']")
    _cancel_hold_locator = (By.CSS_SELECTOR, "[data-test-id*='cancel-hold-']")
    _confirm_cancel_hold_locator = (By.CSS_SELECTOR, "[data-test-id='confirm-cancel-hold']")

    @property
    def bib_title(self):
        return self.find_element(*self._bib_title_locator)

    @property
    def bib_subtitle(self):
        return self.find_element(*self._bib_subtitle_locator)

    @property
    def bib_author(self):
        return self.find_element(*self._bib_author_locator)

    @property
    def add_to_shelf_combo_button(self):
        return self.find_element(*self._add_to_shelf_combo_button_locator)

    @property
    def add_to_shelf_success_message(self):
        return self.find_element(*self._add_to_shelf_success_message_locator)

    @property
    def is_add_to_shelf_success_message_displayed(self):
        return self.find_element(*self._add_to_shelf_success_message_locator).is_displayed()

    @property
    def on_shelf_button(self):
        return self.find_element(*self._on_shelf_button_locator)

    @property
    def is_on_shelf_button_displayed(self):
        return self.find_element(*self._on_shelf_button_locator).is_displayed()

    @property
    def on_shelf_dropdown_button(self):
        return self.find_element(*self._on_shelf_dropdown_button_locator)

    @property
    def remove_from_shelves(self):
        return self.find_element(*self._remove_from_shelves_locator)

    @property
    def request_this_download(self):
        return self.find_element(*self._request_this_download_locator)

    @property
    def is_request_this_download_displayed(self):
        try:
            return self.find_element(*self._request_this_download_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def place_digital_hold(self):
        return self.find_element(*self._place_digital_hold_locator)

    @property
    def is_place_digital_hold_displayed(self):
        try:
            return self.find_element(*self._place_digital_hold_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def place_hold(self):
        return self.find_element(*self._place_hold_locator)

    @property
    def is_place_hold_displayed(self):
        try:
            return self.find_element(*self._place_hold_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def pickup_location(self):
        return self.find_element(*self._pickup_location_locator)

    @property
    def alert(self):
        return self.find_element(*self._alert_locator)

    @property
    def is_alert_displayed(self):
        try:
            return self.find_element(*self._alert_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def item_format(self):
        return self.find_element(*self._item_format_text_locator)

    @property
    def check_availability(self):
        return self.find_element(*self._check_availability_locator)

    @property
    def availability_status(self):
        return self.find_element(*self._availability_status_locator)

    @property
    def is_availability_status_displayed(self):
        try:
            return self.find_element(*self._availability_status_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def confirm_hold(self):
        return self.find_element(*self._confirm_hold_locator)

    @property
    def is_confirm_hold_displayed(self):
        try:
            return self.find_element(*self._confirm_hold_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def cancel_hold(self):
        return self.find_element(*self._cancel_hold_locator)

    @property
    def is_cancel_hold_displayed(self):
        try:
            return self.find_element(*self._cancel_hold_locator)
        except NoSuchElementException:
            return False

    @property
    def confirm_cancel_hold(self):
        return self.find_element(*self._confirm_cancel_hold_locator)

    @property
    def grouped_search_items(self):
        return [SearchResultItem(self, element) for element in self.find_elements(*self._grouped_search_item_locator)]
