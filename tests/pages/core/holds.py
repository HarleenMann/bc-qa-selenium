from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage
from pages.core.components.on_hold_item import OnHoldItem
from pypom import Page, Region

# https://<library>.<environment>.bibliocommons.com/holds/index/not_yet_available
class HoldsPage(BasePage):

    URL_TEMPLATE = "/holds/index/not_yet_available"

    _heading_locator = (By.CLASS_NAME, "visible-lg") # "On Hold" heading
    _on_hold_item_locator = (By.CLASS_NAME, "listItem")
    _status_locator = (By.CLASS_NAME, "utility_bar_status")
    _empty_page_locator = (By.CLASS_NAME, "empty_page")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def items(self):
        return [OnHoldItem(self, element) for element in self.find_elements(*self._on_hold_item_locator)]

    @property
    def date_picker(self):
        return self.DatePicker(self)

    @property
    def status(self):
        return self.find_element(*self._status_locator)

    @property
    def empty_page(self):
        return self.find_element(*self._empty_page_locator)

    @property
    def is_empty_page_displayed(self):
        try:
            return self.find_element(*self._empty_page_locator).is_displayed()
        except NoSuchElementException:
            return False

    class DatePicker(Region):

        _suspend_date_picker_locator = (By.ID, "suspend_start_date_root")
        _today_locator = (By.CLASS_NAME, "picker__button--today")

        # @property
        # def loaded(self):
        #     try:
        #
        #     except NoSuchElementException:
        #         return False

        @property
        def today(self):
            # TODO: Fix this to support the date started picker too:
            try:
                if self.find_element(*self._suspend_date_picker_locator).is_displayed():
                    return self.find_elements(*self._today_locator)[1]
                else: return False
            except NoSuchElementException:
                return False
