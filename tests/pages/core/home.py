# from selenium.webdriver.common.by import By
from .base import BasePage

class HomePage(BasePage):

    @property
    def loaded(self):
        return self.seed_url in self.selenium.current_url

    pass
