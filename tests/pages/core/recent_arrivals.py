from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage
from pypom import Region

# https://<library>.<environment>.bibliocommons.com/explore/recent_arrivals{query_string}
class RecentArrivalsPage(BasePage):

    URL_TEMPLATE = "/explore/recent_arrivals{query_string}"   # Usage: RecentArrivalsPage(self.driver, base_url, query_string = [QueryString]).open()

    _root_locator = (By.CSS_SELECTOR, "[data-test-id='recent_arrivals_page']")
    _dropdown_list_item_locator = (By.CSS_SELECTOR, "li[data-original-index]")
    _format_locator = (By.CSS_SELECTOR, "[testid='select_FORMAT'] + div")
    _content_locator = (By.CSS_SELECTOR, "[testid='select_FICTION_TYPE'] + div")
    _audience_locator = (By.CSS_SELECTOR, "[testid='select_AUDIENCE'] + div")
    _language_locator = (By.CSS_SELECTOR, "[testid='select_LANGUAGE'] + div")
    _apply_locator = (By.CSS_SELECTOR, "[data-js='apply-filters']")
    _no_matching_results_locator = (By.CSS_SELECTOR, "[class='recent_arrivals_list'] > span")
    _explore_panel_locator = (By.CSS_SELECTOR, "[class*='cp_explore_panel']")
    _on_order_locator = (By.CSS_SELECTOR, "[data-key='on_order']")
    _results_locator = (By.CSS_SELECTOR, ".carousel_item")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._root_locator).is_present()
        except NoSuchElementException:
            return False

    @property
    def is_results_displayed(self):
        try:
            return self.find_element(*self._results_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def format(self):
        return self.find_element(*self._format_locator)

    @property
    def content(self):
        return self.find_element(*self._content_locator)

    @property
    def audience(self):
        return self.find_element(*self._audience_locator)

    @property
    def language(self):
        return self.find_element(*self._language_locator)

    # def set_format_to(self, format):
    #     dropdown = self.format
    #     dropdown.click()
    #     options = dropdown.find_elements(*self._dropdown_list_item_locator)
    #     for option in options:
    #         if option.text == format:
    #             option.click()
    #             break

    def select_from_dropdown(self, list, value):
        if list == 'format':
            dropdown = self.format
        elif list == 'content':
            dropdown = self.content
        elif list == 'audience':
            dropdown = self.audience
        elif list == 'language':
            dropdown = self.language
        else:
            raise NoSuchElementException("List name given is not valid for this page.")
        dropdown.click()
        options = dropdown.find_elements(*self._dropdown_list_item_locator)
        for option in options:
            if option.text == value:
                option.click()
                break

    @property
    def apply(self):
        return self.find_element(*self._apply_locator)

    @property
    def no_matching_results(self):
        return self.find_element(*self._no_matching_results_locator)

    @property
    def is_no_matching_results_displayed(self):
        return self.find_element(*self._no_matching_results_locator).is_displayed()

    @property
    def result(self):
        return self.Result(self)

    @property
    def results(self):
        return [self.Result(self, element) for element in self.find_elements(*self._results_locator)]

    class Result(Region):
        _jacket_cover_link_locator = (By.CSS_SELECTOR, ".carousel_item a.jacketCoverLink")

        @property
        def jacket_link(self):
            return self.find_element(*self._jacket_cover_link_locator)

    @property
    def panel(self):
        return self.Panel(self)

    @property
    def panels(self):
        return [self.Panel(self, element) for element in self.find_elements(*self._explore_panel_locator)]

    class Panel(Region):
        _title_locator = (By.CLASS_NAME, "title")
        _links_locator = (By.CSS_SELECTOR, "a[class='link']")

        @property
        def title(self):
            return self.find_element(*self._title_locator)

        @property
        def links(self):
            return self.find_elements(*self._links_locator)
