from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
# from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.core.base import BasePage

# https://<library>.<environment>.bibliocommons.com/account/pickup_location
class PickupLocationPage(BasePage):

    URL_TEMPLATE = "/account/pickup_location"

    _first_preference_locator = (By.CSS_SELECTOR, "[data-id='first_preference'")
    _location_dropdown_locator = (By.CSS_SELECTOR, "div[class*='location_preference']")
    _single_click_holds_toggle_locator = (By.CLASS_NAME, "cp_switch")
    _save_changes_locator = (By.CSS_SELECTOR, "[testid='button_save_preferred_branches']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._first_preference_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def first_preference(self):
        return self.find_element(*self._first_preference_locator)

    @property
    def location_dropdown(self):
        return self.find_element(*self._location_dropdown_locator)

    @property
    def single_click_holds_toggle(self):
        return self.find_element(*self._single_click_holds_toggle_locator)

    @property
    def save_changes(self):
        return self.find_element(*self._save_changes_locator)

    def set_location_to(self, location):
        self.first_preference.click()
        # _location_dropdown_locator (EC expects a touple though so has to be passed like this):
        WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div[class*='location_preference']")))
        options = self.location_dropdown.find_elements(By.CSS_SELECTOR, "[data-original-index] span[class='text']")
        for option in options:
            if option.get_attribute('innerHTML') == location:
                option.click()
                break
