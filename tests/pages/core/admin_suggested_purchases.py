from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage
from pages.core.components.overlay import Overlay
from pypom import Region

# https://<library>.<environment>.bibliocommons.com/suggested_purchases
class AdminSuggestedPurchasesPage(BasePage):

    URL_TEMPLATE = "/bcadmin/suggested_purchases"

    _root_locator = (By.CSS_SELECTOR, "[data-test-id='bcadmin_page']")
    _search_suggestions_box_locator = (By.CSS_SELECTOR, "[data-test-id='suggestions-title-query']")
    _search_suggestions_button_locator = (By.CSS_SELECTOR, "[data-test-id='suggestions-title-search-button']")
    _suggestions_list_locator = (By.CLASS_NAME, "listItem")
    _deny_suggestion_button_locator = (By.CSS_SELECTOR, "[data-test-id='deny_suggestion_button']")
    _zero_suggestions_locator = (By.CSS_SELECTOR, "[class='layout_wrapper no_results']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._root_locator).is_present()
        except NoSuchElementException:
            return False

    @property
    def search_suggestions_box(self):
        return self.find_element(*self._search_suggestions_box_locator)

    @property
    def is_search_suggestions_box_displayed(self):
        try:
            return self.find_element(*self._search_suggestions_box_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def search_suggestions_button(self):
        return self.find_element(*self._search_suggestions_button_locator)

    @property
    def suggestions_list(self):
        return [(self, element) for element in self.find_elements(*self._suggestions_list_locator)]

    @property
    def is_suggestions_list_displayed(self):
        try:
            return self.find_elements(*self._suggestions_list_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def deny_suggestion_button(self):
        return self.find_element(*self._deny_suggestion_button_locator)

    @property
    def zero_suggestions(self):
        return self.find_element(*self._zero_suggestions_locator)

    @property
    def is_zero_suggestions_displayed(self):
        try:
            return self.find_element(*self._zero_suggestions_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def overlay(self):
        return Overlay(self)
