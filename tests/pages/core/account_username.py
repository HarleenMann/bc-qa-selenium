from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage

# https://<library>.<environment>.bibliocommons.com/account/username
class AccountUsernamePage(BasePage):

    URL_TEMPLATE = "/account/username"

    _username_label_locator = (By.CSS_SELECTOR, "[for='user_name_field']")
    _username_locator = (By.CSS_SELECTOR, "[testid='field_username']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._username_label_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def username(self):
        return self.find_element(*self._username_locator)

    @property
    def is_username_displayed(self):
        try:
            return self.find_element(*self._username_locator).is_displayed()
        except NoSuchElementException:
            return False
