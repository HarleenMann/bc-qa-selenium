import pytest
import allure
import sure
import sys
sys.path.append('tests')

from utils.bc_data_layer import DataLayer
from pages.core.home import HomePage


@pytest.mark.analytics
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("Datalayer is seeded with site-specific data")
# @allure.testcase("", "TestRail")
class TestSiteSpecificData:
    def test_SiteSpecificData(self):
        self.datalayer = DataLayer(self.driver)
        base_url = "https://chipublib.demo.bibliocommons.com"
        self.home_page = HomePage(self.driver, base_url).open()

        seed_keys = [
            'bc.siteId',
            'bc.libraryId',
            'bc.domain',
            'bc.externalGaId',
            'bc.gaCrossDomainNames',
            'bc.gaCrossDomainEnabled',
            'bc.cmsEnabled',
            'bc.cmsUrl',
            'bc.crazyEggId'
        ]
        seed = self.datalayer.get_data_layer()[0]
        for key in seed_keys:
            seed.should.have.key(key)
